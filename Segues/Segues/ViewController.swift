//
//  ViewController.swift
//  Segues
//
//  Created by Adecco on 14/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textLabel: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("Will Appear")
        textLabel.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Did Appear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("Did Disappear")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "YellowVC" {
            let destVC = segue.destination as! YellowViewController
            if let txtToSend = textLabel.text{
                destVC.configureVC(data: txtToSend)
            }
        }
    }

}

