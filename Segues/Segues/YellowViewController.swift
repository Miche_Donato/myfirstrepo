//
//  YellowViewController.swift
//  Segues
//
//  Created by Adecco on 14/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class YellowViewController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    
    var data: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(data)
        textLabel.text = data
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureVC(data: String) -> Void{
        self.data = data
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
