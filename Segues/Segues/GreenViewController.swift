//
//  GreenViewController.swift
//  Segues
//
//  Created by Adecco on 14/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class GreenViewController: UIViewController {

    @IBAction func goToYellow(_ sender: UIButton) {
        if let nc = navigationController {
            
            //nc.popToRootViewController(animated: true)
            // oppure usare queste due istruzioni
            let stack: [UIViewController] = nc.viewControllers
            nc.popToViewController(stack[0], animated: true)
            print(stack)
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
