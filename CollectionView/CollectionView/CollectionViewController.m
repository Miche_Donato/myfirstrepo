//
//  CollectionViewController.m
//  CollectionView
//
//  Created by Adecco on 05/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "CollectionViewController.h"
#import "CollectionViewCell.h"

#import "Contact.h"

@interface CollectionViewController ()

@property NSArray *contacts;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    
    UICollectionViewFlowLayout *flowLayout = self.collectionView.collectionViewLayout;
    flowLayout.minimumLineSpacing = 300;
    
    self.collectionView.collectionViewLayout = flowLayout;
    
    NSArray *males = @[
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       ];
    
    NSArray *females = @[
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"]
                         ];
    
    self.contacts = @[males, females];
    
    self.navigationItem.title = @"Contacts";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.contacts.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.contacts[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELLID" forIndexPath:indexPath];
    
    // Configure the cell
    
    cell.backgroundColor = [UIColor redColor];
    
    Contact *c = self.contacts[indexPath.section][indexPath.row];
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@", c.name];
    cell.surnameLabel.text = [NSString stringWithFormat:@"%@", c.surname];
    
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
