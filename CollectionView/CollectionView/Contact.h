//
//  Contact.h
//  CollectionView
//
//  Created by Adecco on 05/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property NSString *name;
@property NSString *surname;
@property NSString *sex;
@property NSString *icon;

-(instancetype)initWithName:(NSString *)name
                    surname:(NSString *)surname
                        sex:(NSString *)sex
                   andImage:(NSString *)icon;

-(NSString *)description;

@end
