//
//  CollectionViewCell.m
//  CollectionView
//
//  Created by Adecco on 05/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "CollectionViewCell.h"

@interface CollectionViewCell()

@end

@implementation CollectionViewCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.imageView.image = [UIImage imageNamed:@"ic_person"];
        [self addSubview:self.imageView];
    }
    return self;
}

@end
