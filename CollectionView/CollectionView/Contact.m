//
//  Contact.m
//  CollectionView
//
//  Created by Adecco on 05/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Contact.h"

@implementation Contact

-(instancetype)initWithName:(NSString *)name
                    surname:(NSString *)surname
                        sex:(NSString *)sex
                   andImage:(NSString *)icon{
    
    if(self = [super init]){
        self.name = name;
        self.surname = surname;
        self.sex = sex;
        self.icon = icon;
        return self;
    }
    return nil;
    
}

-(NSString *)description{
    
    NSString *s = [NSString stringWithFormat:@"%@ %@", self.name, self.surname];
    return s;
}

@end
