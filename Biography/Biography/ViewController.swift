//
//  ViewController.swift
//  Biography
//
//  Created by Adecco on 13/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var biographyLabel: UILabel!
    
    var biographyText: String = "Darth Vader, also known as Anakin Skywalker, is a fictional character in the Star Wars universe. Vader appears in the original trilogy as a pivotal figure whose actions drive the plot of the first three films while his past as Anakin Skywalker, and the story of his corruption, is central to the prequel trilogy."

    override func viewDidLoad() {
        super.viewDidLoad()
        biographyLabel.text = biographyText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
