//
//  ContactsTableViewController.m
//  MyTableView
//
//  Created by Adecco on 02/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "ContactsTableViewController.h"
#import "Contact.h"
#import "DetailsViewController.h"

@interface ContactsTableViewController ()

@property NSArray *contacts;

@end

@implementation ContactsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    ;
    
    NSArray *males = @[
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Michele" surname:@"Donato" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Marco" surname:@"Agati" sex:@"male" andImage:@"ic_person"],
                       [[Contact alloc] initWithName:@"Eridon" surname:@"Bramillari" sex:@"male" andImage:@"ic_person"],
                       ];
    
    NSArray *females = @[
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Elisa" surname:@"Stoppani" sex:@"female" andImage:@"ic_person"],
                         [[Contact alloc] initWithName:@"Nijini" surname:@"Boh" sex:@"female" andImage:@"ic_person"]
                         ];
    
    self.contacts = @[males, females];
    
    self.navigationItem.title = @"Contacts";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(Contact *)contactAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    
    NSArray *arr = self.contacts[section];
    
    Contact *contact = arr[row];
    return contact;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.contacts.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.contacts[section] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *s;
    
    if(section == 0){
        s = [NSString stringWithFormat:@"Males"];
    }else{
        s = [NSString stringWithFormat:@"Females"];
    }
    return s;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}

#define IMG_TAG 1000

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELLID" forIndexPath:indexPath];
    
    Contact *contact = [self contactAtIndexPath:indexPath];
    
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    
    NSString *s = [NSString stringWithFormat:@"%ld %ld %@", section, row, contact];
    cell.textLabel.text = s;
    
    if(section == 1){
        UIColor *c = [UIColor purpleColor];
        cell.backgroundColor = c;
    }else{
        cell.backgroundColor = [UIColor blueColor];
    }
    
    UIImage *image = [UIImage imageNamed:contact.icon];
    
    UIImageView *imgView = [cell viewWithTag:IMG_TAG];
    if(imgView == nil){
        CGFloat X = cell.frame.size.width - 110;
        imgView = [[UIImageView alloc]initWithFrame:CGRectMake(X, 10, 100, 100)];
        imgView.tag = IMG_TAG;
        [cell addSubview:imgView];
    }
    imgView.image = image;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    DetailsViewController *dvc = [segue destinationViewController];
    
    NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
    
    Contact *c = [self contactAtIndexPath:indexPath];
    
    [dvc getContact:c];
}


@end
