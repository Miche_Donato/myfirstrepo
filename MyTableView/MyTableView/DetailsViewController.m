//
//  DetailsViewController.m
//  MyTableView
//
//  Created by Adecco on 02/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "DetailsViewController.h"


@interface DetailsViewController ()

@property Contact *contact;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation DetailsViewController

-(void)getContact:(Contact *)c{
    self.contact = c;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage *image = [UIImage imageNamed:self.contact.icon];
    self.imgView.image = image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
