//
//  Places.m
//  Panino
//
//  Created by Adecco on 06/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Places.h"

@implementation Places

- (instancetype) initWithDictFromJSON:(NSDictionary*)d {
    if (self = [super init]) {
        self.formatted_address = d[@"formatted_address"];
        
        NSArray * address_components = d[@"address_components"];
        self.address = address_components[0][@"long_name"];
        self.city = address_components[1][@"long_name"];
        self.prov = address_components[3][@"short_name"];
        self.region = address_components[4][@"long_name"];
        self.country = address_components[5][@"short_name"];
        
        NSDictionary * geometry = d[@"geometry"];
        NSDictionary * location = geometry[@"location"];
        // NSLog(@"%@", geometry);
        NSString * lat = location[@"lat"];
        NSString * lng = location[@"lng"];
        self.coords = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
    }
    return self;
}

#pragma mark MKAnnotation

- (CLLocationCoordinate2D) coordinate {
    return self.coords;
}

- (NSString *) title {
    return [NSString stringWithFormat:@"%@ %@", self.prov, self.city];
}

- (NSString *) subtitle {
    return self.address;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"%@, %@, %@, %@, %@", self.country, self.region, self.prov, self.city, self.address];
}

@end
