//
//  ViewController.m
//  Panino
//
//  Created by Adecco on 05/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Places.h"
#import "ViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MKMapView *map;

// @property CGRect originalFrame;
@property NSTimer * timer;
@property NSMutableArray<Places *> * places;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panino;

@end

@implementation ViewController

- (IBAction)toggleIt:(UIBarButtonItem *)sender {
    if ([self isVisible]) {
        [self hide: YES];
    }
    else {
        [self showLateralTable];
    }
}

- (BOOL) isVisible {
    /* CGRect r = self.tableView.frame;
    if (r.origin.x<0) {
        return NO;
    }
    else {
        return YES;
    } */
    
    CGFloat x = self.panino.constant;
    if (x<=0) {
        return NO;
    }
    else {
        return YES;
    }
}

- (void) hide: (BOOL) animated{
    [self.timer invalidate];
    self.timer = nil;
    
    // CGFloat w = self.originalFrame.size.width;
    // CGFloat h = self.originalFrame.size.height;
    
    [self.view layoutIfNeeded];
    
    if (animated) {
        [UIView animateWithDuration:1 animations:^{
            // self.tableView.frame = CGRectMake(-w, 0, w, h);
            self.panino.constant=0;
            [self.view layoutIfNeeded];
        }];
    }
    else {
        // self.tableView.frame = CGRectMake(-w, 0, w, h);
        self.panino.constant=0;
        [self.view layoutIfNeeded];
    }
    
}

- (void) showLateralTable {
    
    [self.view layoutIfNeeded];
    
    [UIView animateWithDuration:1 animations:^{
        // self.tableView.frame = self.originalFrame;
        self.panino.constant = 200;
        [self.view layoutIfNeeded];
    }];
    [self startMyTimer];
}

- (void) startMyTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self hide:YES];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.places.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tabella" forIndexPath:indexPath];
    
    //cell.textLabel.text = [NSString stringWithFormat:@"cella %ld", (long) indexPath.row + 1];
    Places * p = self.places[indexPath.row];
    // NSString * s = p.city;
    // NSLog(s);
    cell.textLabel.text = [p description];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hide: NO];
    
    NSInteger row = indexPath.row;
    Places * p = self.places[row];
    [self goHere:p.coords];
}

- (void) goHere: (CLLocationCoordinate2D) initialPos {
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = MKCoordinateRegionMake(initialPos, span);
    [self.map setRegion:region];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // self.originalFrame = self.tableView.frame;
    [self hide: NO];
    
    self.places = [[NSMutableArray alloc]init];
    CLLocationCoordinate2D initialPos = CLLocationCoordinate2DMake(45.44, 9.1698);
    [self goHere:initialPos];
    [self downloadData];
}

- (void) downloadData {
    #define GOOGLE_URL @"https://maps.googleapis.com/maps/api/geocode/json"
    NSString * address = @"piazza+duomo";
    NSString * options = @"sensor=true";
    
    // Passo 0: creare URI.
    NSString * s = [NSString stringWithFormat:@"%@?address=%@&%@", GOOGLE_URL, address, options];
    
    // Passo 1: creare URL.
    NSURL * URL = [NSURL URLWithString:s];
    
    // Passo 2: creare la Request.
    NSURLRequest * request = [NSURLRequest requestWithURL:URL];
    
    // Passo 3: creare la Session (come shared session).
    NSURLSession * session = [NSURLSession sharedSession];
    
    // Passo 4: creare un Task (Per ora di download dati in memoria. Potrebbe essere di download su disco o di upload).
    // Usare autocompletion.
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request
                                             completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error!=nil) {
            NSLog(@"%@", error);
        }
        else {
            [self parseData: data];
        }
        
        // Da fare SEMPRE nelle chiamate di rete!!!!!!! ASSOLUTAMENTE IMPORTANTISSIMO!! Altrimenti comportamenti non previsti & crash!
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLateralTable];
            
            [self.map addAnnotations:self.places];
            [self.tableView reloadData];
        });
    }];
    
    // Non parte nulla perchè manca il Task Resume.
    [task resume];
}

- (void) parseData: (NSData*) data {
     /* NSString * s = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
     if (s==nil) {
     s = [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding];
     }
     NSLog(@"%@", s); */
    
    NSError * error;
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    // Dict in sto caso è un dizionario che contiene un vettore result, che a sua volta è un vettore di altri 10 dizionari.
    // NSLog(@"%@", dict);
    NSArray * results = dict[@"results"];
    // Estrapolo il vettore dal dizionario padre.
    // NSLog(@"%@", results);
    
    // Ora prendo i singoli dizionari figli
    for (NSDictionary * d in results) {
        Places * p = [[Places alloc]initWithDictFromJSON:d];
        if (p) {
            [self.places addObject:p];
        }
    }
}

#pragma mark map delegate

- (nullable MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    // MKAnnotationView * av = [[MKAnnotationView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    MKPinAnnotationView * pin = (MKPinAnnotationView *) [self.map dequeueReusableAnnotationViewWithIdentifier:@"PIN_ID"];
    if (pin==nil) {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PIN_ID"];
        pin.canShowCallout = YES;
    }
    else {
        pin.annotation = annotation;
    }
    
    return pin;
}

@end
