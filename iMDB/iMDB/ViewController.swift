//
//  ViewController.swift
//  iMDB
//
//  Created by Adecco on 15/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var directorField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    @IBOutlet weak var durationField: UITextField!
    
    let dm = DataManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        dm.loadAllMovies()
    }
    
    @IBAction func update(_ sender: Any) {
        if let index = tableView.indexPathForSelectedRow {
            if titleField.text != "" && directorField.text != "" && yearField.text != "" && durationField.text != "" {
                
                let title = titleField.text!
                let director = directorField.text!
                let year = yearField.text!
                let duration = durationField.text!
                
                dm.updateMovieWithIndex(index: index.row, title: title, director: director, year: Int(year)!, duration: Int(duration)!)
                tableView.reloadData()
            }
        }
        resetField()
    }
    
    @IBAction func search(_ sender: Any) {
        
        if let searchTxt = titleField.text {
            if searchTxt == "" {
                dm.loadAllMovies()
            }else{
                dm.loadMoviesWithTitle(title: searchTxt)
            }
            tableView.reloadData()
        }
    }
    
    @IBAction func onSaveBtnTapped(_ sender: Any) {
        if let title = titleField.text, let director = directorField.text, let year = yearField.text, let duration = durationField.text {
            
            let intYear = Int(year)!
            let intDuration = Int(duration)!
            
            dm.storeMovie(title: title, director: director, year: intYear, duration: intDuration)
            dm.loadAllMovies()
            tableView.reloadData()
            
            resetField()
        }
    }
    
    private func resetField(){
        titleField.text = ""
        directorField.text = ""
        yearField.text = ""
        durationField.text = ""
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dm.movies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath)
        
        var movies = dm.movies
        
        cell.textLabel?.text = movies[indexPath.row].title
        cell.detailTextLabel?.text = movies[indexPath.row].director
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            //let titleMovie = dm.movies[indexPath.row].title
            //dm.deleteMovieWithTitle(title: titleMovie!)
            dm.deleteMovieWithIndex(index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let movie = dm.movies[indexPath.row]
        
        titleField.text = movie.title
        directorField.text = movie.director
        yearField.text = String(movie.year)
        durationField.text = String(movie.duration)
    }

}

