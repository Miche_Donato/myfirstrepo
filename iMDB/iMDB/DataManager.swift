//
//  DataManager.swift
//  iMDB
//
//  Created by Adecco on 15/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// Singleton
class DataManager {
    
    static let shared = DataManager()
    
    private var context: NSManagedObjectContext
    
    private var _movies: [Movie]
    
    var movies: [Movie] {
        return _movies
    }
    
    private init(){
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
        _movies = [Movie]()
    }
    
    func storeMovie(title: String, director: String, year: Int, duration: Int) {
        // entity è il nostro DB
        let entity = NSEntityDescription.entity(forEntityName: "Movie", in: context)
        let newMovie = Movie(entity: entity!, insertInto: context)
        newMovie.title = title
        newMovie.director = director
        newMovie.year = Int16(year)
        newMovie.duration = Int16(duration)
        
        do {
            try context.save()
            print("Movie successfully saved")
        }catch let error {
            print("Error while saving new movie: \(error)")
        }
        
    }
    
    func loadAllMovies() {
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
        loadFetchRequest(fetchRequest: fetchRequest)
    }
    
    func loadMoviesWithTitle(title: String) {
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
        
        // [cd] 'c' per il case sensitive e 'd' per le lettere accentate
        let predicate = NSPredicate(format: "title = [cd] %@", title)
        fetchRequest.predicate = predicate
        
        loadFetchRequest(fetchRequest: fetchRequest)
    }
    
    func deleteMovieWithTitle(title: String) {
        
        loadMoviesWithTitle(title: title)
        if movies.count > 0 {
            let movie = movies[0]
            context.delete(movie)
            do {
                try context.save()
                loadAllMovies()
            }catch let error {
                print("Failed to delete movie: \(error)")
            }
        }
        
    }
    
    func deleteMovieWithIndex(index: Int) {
        guard index < movies.count else {
            return
        }
        let movie = movies[index]
        context.delete(movie)
        do {
            try context.save()
            loadAllMovies()
        }catch let error {
            print("Failed to delete movie: \(error)")
        }
    }
    
    func updateMovie(title: String, director: String, year: Int, duration: Int){
        loadMoviesWithTitle(title: title)
        if movies.count > 0 {
            let movie = movies[0]
            movie.director = director
            movie.year = Int16(year)
            movie.duration = Int16(duration)
            
            do {
                try movie.managedObjectContext?.save()
                loadAllMovies()
            }catch let error {
                print("Failed to update movie: \(error)")
            }
            
        }
    }
    
    func updateMovieWithIndex(index: Int, title: String, director: String, year: Int, duration: Int){
        guard index < movies.count else {
            return
        }
        let movie = movies[index]
        movie.title = title
        movie.director = director
        movie.year = Int16(year)
        movie.duration = Int16(duration)
        
        do {
            try context.save()
            loadAllMovies()
        }catch let error {
            print("Failed to update movie: \(error)")
        }
    }
    
    private func loadFetchRequest(fetchRequest: NSFetchRequest<Movie>) {
        do {
            let movies = try context.fetch(fetchRequest)
            
            /*
            guard movies.count > 0 else {
                print("There are no movies stored")
                return
            }
            */
            
            _movies = movies
            print("Movies successfully fetched")
            
        }catch let error {
            print("Error occurred while fetching movies: \(error)")
        }
    }
}
