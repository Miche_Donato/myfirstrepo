//
//  ViewController.swift
//  BitCoinGenerator
//
//  Created by Adecco on 12/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var coinBtn: UIButton!
    @IBOutlet weak var startBotton: UIButton!
    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var amountTxt: UITextField!
    
    var totalClicks = 0
    var clicks = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func onStartBtnPressed (_ sender: Any) {
        if let validAmount = amountTxt.text, let validNumber = Int (validAmount) {
            totalClicks = validNumber
            toggleHiddenView()
            clicks = 0
        }
    }
    
    @IBAction func onCoinButtonPressed(_ sender: Any) {
        if (clicks < totalClicks) {
            clicks += 1
            updateOutputLabel(counter: clicks)
        }
        else {
            toggleHiddenView()
            amountTxt.text = ""
            updateOutputLabel(counter: 0)
        }
    }
    
    func updateOutputLabel (counter: Int) -> Void {
        outputLabel.text = "\(counter) BIT COINS"
    }
    
    func toggleHiddenView() -> Void {
        coinBtn.isHidden = !coinBtn.isHidden
        startBotton.isHidden = !startBotton.isHidden
        outputLabel.isHidden = !outputLabel.isHidden
        amountTxt.isHidden = !amountTxt.isHidden
    }

}
