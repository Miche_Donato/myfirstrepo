//
//  ViewController.swift
//  WebView
//
//  Created by Adecco on 14/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBAction func leftButton(_ sender: Any) {
        lastUrl = urlFP
        loadSite(urlStr: urlFP)
    }
    @IBAction func rightButton(_ sender: Any) {
        lastUrl = urlGN
        loadSite(urlStr: urlGN)
    }

    var webView: WKWebView!
    
    let urlFP: String = "http://www.formulapassion.it/"
    let urlGN: String = "https://news.google.it/"
    var lastUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        webView = WKWebView()
        containerView.addSubview(webView)
        
        lastUrl = urlFP
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(adaptWebView), name: Notification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let frame = CGRect(x: containerView.bounds.origin.x, y: containerView.bounds.origin.y, width: containerView.bounds.width, height: containerView.bounds.height)
        
        webView.frame = frame
        loadSite(urlStr: urlFP)
    }
    
    func adaptWebView(){
        let frame = CGRect(x: containerView.bounds.origin.x, y: containerView.bounds.origin.y, width: containerView.bounds.width, height: containerView.bounds.height)
        
        webView.frame = frame
        loadSite(urlStr: lastUrl!)
    }
    
    func loadSite(urlStr: String){
        let url = URL(string: urlStr)!
        let urlRequest = URLRequest(url: url)
        webView.load(urlRequest)
    }
}

