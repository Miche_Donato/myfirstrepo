//
//  ViewController.swift
//  EnumTest
//
//  Created by Adecco on 13/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var quoteLabel: UILabel!
    
    let vaderQuote = "Luke, I am your father"
    let salviniQuote = "Ruspe!"
    let marilynQuote = "Happy birthday Mr. President"
    
    enum Characters: Int {
        case vader = 0
        case salvini = 1
        case marilyn = 2
    }
    
    @IBAction func onButtonTapped (sender: UIButton!) {
        switch sender.tag {
        case Characters.vader.rawValue:
            quoteLabel.text = vaderQuote
        case Characters.salvini.rawValue:
            quoteLabel.text = salviniQuote
        case Characters.marilyn.rawValue:
            quoteLabel.text = marilynQuote
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

}
