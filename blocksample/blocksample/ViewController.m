//
//  ViewController.m
//  blocksample
//
//  Created by ing.conti on 05/12/2016.
//  Copyright © 2016 ingconti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property CGRect originalFrame;

@property NSTimer *timer;

@end


@implementation ViewController

- (IBAction)toggleIt:(UIBarButtonItem *)sender {
	
	if ([self isVisible]){
		[self hide: YES];
	}else{
		[self show];
        [self startMyTimer];
	}
}

-(void)startMyTimer{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self hide:YES];
    }];
}

-(BOOL)isVisible{
	
	CGRect r = self.tableView.frame;
	if (r.origin.x<0)
		return NO;
	return YES;
}

-(void)hide:(BOOL)animated{
    
    [self.timer invalidate];
    
	CGFloat W = self.originalFrame.size.width;
	CGFloat H = self.originalFrame.size.height;
	if (animated){
		[UIView animateWithDuration: 1 animations:^{
			
			self.tableView.frame = CGRectMake(-W, 0, W, H);
			
		}];
	}else{
		self.tableView.frame = CGRectMake(-W, 0, W, H);

	}
}

-(void)show {
	[UIView animateWithDuration:1 animations:^{
		
		self.tableView.frame = self.originalFrame;
		
	}];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.originalFrame = self.tableView.frame;
	[self hide:NO];
}




#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"paninoCell" forIndexPath:indexPath];
 
    // Configure the cell...
	cell.textLabel.text = [NSString stringWithFormat:@"cella %ld", (long)indexPath.row];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[self hide: NO];
}

@end
