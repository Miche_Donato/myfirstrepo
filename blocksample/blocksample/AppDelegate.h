//
//  AppDelegate.h
//  blocksample
//
//  Created by ing.conti on 05/12/2016.
//  Copyright © 2016 ingconti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

