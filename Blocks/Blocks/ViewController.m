//
//  ViewController.m
//  Blocks
//
//  Created by Adecco on 05/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *frameView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property CGRect originalFrame;

@end

// Prototipi
int (^add)(int, int);
void (^doIt)(void);
typedef int (^Block)(int);

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    doIt = ^(void){
        NSLog(@"Hello, World!");
    };
    
    doIt();
    
    
    add = ^(int a, int b){
        return a + b;
    };
    int x = add(10, 10);
    
    NSLog(@"%d", x);
    /*
    Block b = ^(int p){
        printf("Ciao");
        return p;
    };
    b(3);
    
    ripetiBlocco(^(int p){
        printf("%d", p);
        return 0;
    });
 
    
    CGRect r = self.frameView.frame;
    
    [UIView animateWithDuration:1 animations:^{
        //CGRect newRect = CGRectMake(10, 600, 10, 10);
        //self.frameView.frame = newRect;
        self.frameView.alpha = 0;
    }completion:^(BOOL finished) {
        //self.frameView.frame = r;
        self.frameView.alpha = 1;
    }];
    */
    
    self.originalFrame = self.tableView.frame;
    CGFloat W = self.originalFrame.size.width;
    CGFloat H = self.originalFrame.size.height;
    
    self.tableView.frame = CGRectMake(-W, 0, W, H);
}

/*
-(void) ripetiBlocco(Block bk){
    
    for(int i = 0; i < 10; i++){
        bk(i);
    }
    
}
*/


@end





