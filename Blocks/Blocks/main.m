//
//  main.m
//  Blocks
//
//  Created by Adecco on 05/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
