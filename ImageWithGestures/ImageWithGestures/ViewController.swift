//
//  ViewController.swift
//  ImageWithGestures
//
//  Created by Adecco on 16/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    @IBAction func btnAction(_ sender: Any) {
        present(imgPicker, animated: true, completion: nil)
    }
    
    var imgPicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        imgPicker = UIImagePickerController()
        imgPicker.delegate = self
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgPicker.dismiss(animated: true, completion: nil)
            imgView.image = tempImage
        }
    }
    @IBAction func pinchToZoom(_ sender: UIPinchGestureRecognizer) {
        imgView.transform = imgView.transform.scaledBy(x: sender.scale, y: sender.scale)
    }
    

}

