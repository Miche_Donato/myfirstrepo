//
//  ViewController.m
//  EuroConverter
//
//  Created by Adecco on 01/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *euroField;
@property (weak, nonatomic) IBOutlet UITextField *dollarField;
@property (weak, nonatomic) IBOutlet UIButton *upArrow;
@property (weak, nonatomic) IBOutlet UIButton *downArrow;

@end

@implementation ViewController

-(CGFloat)floatValue:(UITextField*)tf{
    NSString* s = tf.text;
    CGFloat n = [s doubleValue];
    return n;
}

- (IBAction)startEditing:(UITextField *)sender {
    sender.textColor = [UIColor blackColor];

}

- (IBAction)upAction:(UIButton *)sender {
    
    if([self.dollarField.text isEqualToString:@""]){
        return;
    }
    
    CGFloat dollar = [self floatValue: self.dollarField];
    
    if(dollar < 0){
        self.dollarField.textColor = [UIColor redColor];
        return;
    }
    
    CGFloat euro = dollar / 1.0616;
    
    self.euroField.text = [NSString stringWithFormat:@"%.2f", euro];
    
    [self.dollarField resignFirstResponder];
}

- (IBAction)downAction:(UIButton *)sender {
    
    if([self.euroField.text isEqualToString:@""]){
        return;
    }
    
    CGFloat euro = [self floatValue: self.euroField];
    
    if(euro < 0){
        self.euroField.textColor = [UIColor redColor];
        return;
    }
    
    CGFloat dollar = euro * 1.0616;
    
    self.dollarField.text = [NSString stringWithFormat:@"%.2f", dollar];
    
    [self.euroField resignFirstResponder];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
