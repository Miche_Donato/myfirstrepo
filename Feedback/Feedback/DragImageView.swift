//
//  DragImageView.swift
//  Feedback
//
//  Created by Lev Barki on 13/12/16.
//  Copyright © 2016 Lev Barki. All rights reserved.
//

import Foundation
import UIKit

class DragImageView: UIImageView {
    
    var startingPos: CGPoint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        startingPos = self.center
        //print("x \(startingPos?.x) y:\(startingPos?.y)")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: self.superview)
            self.center = position
            //print("x: \(position.x) \(position.y)")
            // Così position continua a cambiare perché segue il tuo tocco
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != nil {
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name.init(rawValue: "onTouchEnded"), object: nil)
        }
    }
    
    func resetPosition(){
        if startingPos != nil{
            self.center = startingPos!
        }
    }

}
