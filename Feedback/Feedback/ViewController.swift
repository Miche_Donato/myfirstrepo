//
//  ViewController.swift
//  Feedback
//
//  Created by Adecco on 13/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var fb4ImgView: DragImageView!
    @IBOutlet weak var fb3ImgView: DragImageView!
    @IBOutlet weak var fb2ImgView: DragImageView!
    @IBOutlet weak var fb1ImgView: DragImageView!
    @IBOutlet weak var pictureImgView: UIImageView!
    @IBOutlet weak var feedbackLabel: UILabel!
    @IBAction func onOkBottonTapped(_ sender: UIButton) {
        fb1ImgView.resetPosition()
        fb2ImgView.resetPosition()
        fb3ImgView.resetPosition()
        fb4ImgView.resetPosition()
        
        feedbackLabel.text = "Drag icons to get feedback"
        
        if let nextPicture = nextPicture(){
            pictureImgView.image = nextPicture
        }
        
    }
    
    func nextPicture() -> UIImage? {
        if let nextPic:UIImage = UIImage(named: "pic\(i).jpg"){
            i += 1
            return nextPic
        }
        return nil
    }
    
    var stat1: Int = 0
    var stat2: Int = 0
    var stat3: Int = 0
    var stat4: Int = 0
    var baseDst = 0.0
    var i: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currPicture = nextPicture() {
            pictureImgView.image = currPicture
        }
        pictureImgView.layer.cornerRadius = 5.0
        pictureImgView.clipsToBounds = true
        
        baseDst = getDistance(p1: pictureImgView.center, p2: fb1ImgView.center)
        
        let nc = NotificationCenter.default
        let myNotification = Notification.Name(rawValue: "onTouchEnded")
        nc.addObserver(forName: myNotification, object: nil, queue: nil, using: catchNotification)
        
    }
    
    func catchNotification(notification: Notification) -> Void {
        computeRatings()
        updateStatLabel()
    }
    
    func computeRatings(){
        let dist1 = getDistance(p1: pictureImgView.center, p2: fb1ImgView.center)
        let dist2 = getDistance(p1: pictureImgView.center, p2: fb2ImgView.center)
        let dist3 = getDistance(p1: pictureImgView.center, p2: fb3ImgView.center)
        let dist4 = getDistance(p1: pictureImgView.center, p2: fb4ImgView.center)
        
        stat1 = Int (((baseDst - dist1) / baseDst) * 5 + 1)
        stat2 = Int (((baseDst - dist2) / baseDst) * 5 + 1)
        stat3 = Int (((baseDst - dist3) / baseDst) * 5 + 1)
        stat4 = Int (((baseDst - dist4) / baseDst) * 5 + 1)
    }
    
    func getDistance(p1: CGPoint, p2: CGPoint) -> Double{
        return Double (sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2)))
    }
    
    func updateStatLabel(){
        feedbackLabel.text = "Like: \(stat1) Love: \(stat2) Sad: \(stat3) Angry: \(stat4)"
    }

}
