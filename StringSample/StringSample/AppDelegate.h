//
//  AppDelegate.h
//  StringSample
//
//  Created by Adecco on 30/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

