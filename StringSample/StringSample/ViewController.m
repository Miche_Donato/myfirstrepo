//
//  ViewController.m
//  StringSample
//
//  Created by Adecco on 30/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* s= @"Hello";
    
    NSString* s2 = [[NSString alloc] initWithUTF8String:"World"];
    
    NSString* s3 = [s stringByAppendingFormat:@" %@", s2];
    
    NSLog(@"%@", s3);
    
    NSString* s4 = [s3 uppercaseString];
    
    NSLog(@"%@",s4);
    
    NSString* path = @"/Users/adecco/Documents/gitFolder/file.txt";
    NSString* ext = [path pathExtension];
    NSString* last = [path lastPathComponent];
    NSString* dir = [path stringByDeletingLastPathComponent];
    NSArray* arr = [path componentsSeparatedByString:@"/"];
    
    NSArray* myArray = [NSArray arrayWithObjects:@"Hello", @"World", @"!!!!", nil];
    NSArray* mySimpleArray = @[@"Hello", @"World"];
    
    NSMutableArray* mutableArray = [NSMutableArray new];
    [mutableArray addObject: [@[@"Sally", @"John"] mutableCopy]];
    NSMutableArray* copyOfArray = [mutableArray mutableCopy];
    copyOfArray[0][0] = @[@"Tom"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
