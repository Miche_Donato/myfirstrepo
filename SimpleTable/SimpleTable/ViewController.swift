//
//  ViewController.swift
//  SimpleTable
//
//  Created by Adecco on 13/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var table: UITableView!
    
    // Per far scomparire la status bar
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    var houses = [House]()
    
    let data: [String] = ["Targaryen", "Martell", "Stark", "Lannister", "Greyjoy", "Hodor"]
    let motto = ["Fire and Blood", "Unboud unbent unbroken", "Wintes is coming", "Here me roar!", "We do not sow", "Hodor, Hodor, Hodor"]
    
    var filteredHouses = [House]()
    
    var inSearchMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Invece che fare il collegamento dal Main.Storyboard
        table.delegate = self
        table.dataSource = self
        
        for i in 0..<data.count{
            let imgStr = "\(data[i]).png"
            houses.append(House(name: data[i], motto: motto[i], imgName: imgStr.lowercased()))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode{
            return filteredHouses.count
        }else {
            return houses.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)
        
        if inSearchMode{
            // textLabel è il title
            cell.textLabel?.text = filteredHouses[indexPath.row].name
            // detailTextLabel è il subtitle
            cell.detailTextLabel?.text = filteredHouses[indexPath.row].motto
            cell.imageView?.image = UIImage(named: filteredHouses[indexPath.row].imgName)
        }else{
            // textLabel è il title
            cell.textLabel?.text = houses[indexPath.row].name
            // detailTextLabel è il subtitle
            cell.detailTextLabel?.text = houses[indexPath.row].motto
            cell.imageView?.image = UIImage(named: houses[indexPath.row].imgName)
        }
        
        
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            inSearchMode = false
        }else{
            inSearchMode = true
            let lower = searchText.lowercased()
            filteredHouses = houses.filter({ $0.name.lowercased().range(of: lower) != nil })
        }
        table.reloadData()
    }
}

