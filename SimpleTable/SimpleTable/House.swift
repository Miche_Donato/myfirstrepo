//
//  House.swift
//  SimpleTable
//
//  Created by Adecco on 14/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import Foundation

class House {
    
    private var _name: String
    private var _motto: String
    private var _imgName: String
    
    var name: String {
        return _name
    }
    
    // Stessa cosa
    var motto: String {
        get{
            return _motto
        }
    }
    
    var imgName: String {
        return _imgName
    }
    
    init(name: String, motto: String, imgName: String){
        _name = name
        _motto = motto
        _imgName = imgName
    }
    
    
    
}
