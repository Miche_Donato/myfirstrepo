//
//  ViewController.m
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "ViewController.h"
#import "Rectangle.h"
#import "Circle.h"
#import "Square.h"

#import "CustomView.h"


@interface ViewController ()

//@property MyRectangle *r;
@property (weak, nonatomic) IBOutlet UITextField *xField;
@property (weak, nonatomic) IBOutlet UITextField *yField;
@property (weak, nonatomic) IBOutlet UITextField *baseField;
@property (weak, nonatomic) IBOutlet UITextField *heightField;


@end

@implementation ViewController

- (IBAction)xAction:(UITextField *)sender {
}

- (IBAction)yAction:(UITextField *)sender {
}

- (IBAction)baseAction:(UITextField *)sender {
}

- (IBAction)heightAction:(UITextField *)sender {
}

- (IBAction)buttonAction:(UIButton *)sender {
    
    // Per togliere la keyboard dallo schermo usiamo questo metodo
    // NSObject firstResp =
    [self.xField resignFirstResponder];
    [self.yField resignFirstResponder];
    [self.baseField resignFirstResponder];
    [self.heightField resignFirstResponder];
    
    CGFloat x = [self floatValue:self.xField];
    CGFloat y = [self floatValue:self.yField];
    CGFloat base = [self floatValue:self.baseField];
    CGFloat height = [self floatValue:self.heightField];
    
    Rectangle* r = [[Rectangle alloc]initWithBase:base andHeight:height];
    r.center = CGPointMake(x, y);
    r.color = [UIColor redColor];
    
    CustomView* cv =(CustomView*)self.view;
    [cv addRect:[self rectFromRectangle:r]];
    // Per refreshare il display e far apparire i nuovi quadrati
    [cv setNeedsDisplay];
    
}

-(CGFloat)floatValue:(UITextField*)tf{
    NSString* s = tf.text;
    CGFloat n = [s doubleValue];
    return n;
}



-(CGRect)rectFromRectangle:(Rectangle*)r{
    
    CGRect result = CGRectMake(r.center.x - r.base/2,
                               r.center.y - r.height/2,
                               r.base, r.height);
    return result;
}

// Simile all'onCreate
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // NSLog(@"viewDidLoad");
    [self printLog:@"viewDidLoad"];
    
    DEBUG_LOG(@"ECCOMI");
    
//    MyRectangle *rect = [[MyRectangle alloc]init];
//    rect.height = 100;
//    rect.base = 200;
//    CGFloat area = [rect area];
//    
//    int n = [MyRectangle numberOfAllocatedReacts];
//    
//    rect = nil;
//    
//    n = [MyRectangle numberOfAllocatedReacts];
//    
//    // Per accedere alla proprietà privata r devo usare self.
//    self.r.base = 10;
//    self.r.height = 20;
//    CGFloat area3 = [self.r area];
    
    /*
    CustomView* cv = (CustomView*) self.view;
    
    Rectangle* r = [[Rectangle alloc]initWithBase:50 andHeight:150];
    // Usiamo l'init al posto di usare i setter qui in basso
//    r.height = 10;
//    r.base = 20;
    r.center = CGPointMake(100, 300);
    r.color = [UIColor redColor];
    CGFloat areaRect = [r area];
    
    [cv addRect:[self rectFromRectangle:r]];
    
    
    Rectangle* r2 = [[Rectangle alloc]initWithBase:50 andHeight:100];
    r2.center = CGPointMake(100, 120);
    r2.color = [UIColor greenColor];
    
    [cv addRect:[self rectFromRectangle:r2]];
    
    BOOL intersect = [r intersectWith:r2];
    NSLog(@"%d", intersect);
    
    Rectangle* r3 = [[Rectangle alloc]initWithBase:80 andHeight:100];
    r3.center = CGPointMake(150, 170);
    r3.color = [UIColor greenColor];
    
    [cv addRect:[self rectFromRectangle:r3]];
    
    
    Circle* c = [[Circle alloc]initWithRadius:5];
//    c.radius = 5;
    c.color = [UIColor blueColor];
    CGFloat areaCircle = [c area];
    
    Square* s = [[Square alloc]initWithSide:4];
    CGFloat areaSquare = [s area];
    CGFloat perimeterSquare = [s perimeter];
     */
}

- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Simile all'onStart/onResume
- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
}

// Simile all'onPause/onStop
- (void)viewWillDisappear:(BOOL)animated{
    NSLog(@"viewWillDisappear");
    [super viewWillDisappear:animated];
}

// Simile all'onDestroy
- (void)viewDidUnload{
    NSLog(@"viewDidUnload");
    [super viewDidUnload];
}

- (void) printLog:(NSString*)string{
    NSLog(@"%@", string);
}

@end
