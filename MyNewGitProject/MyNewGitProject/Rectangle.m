//
//  MyRectangle.m
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Rectangle.h"

@interface Rectangle()

// Proprietà e metodi private

@end


@implementation Rectangle

-(instancetype)initWithBase:(CGFloat)base andHeight: (CGFloat)height{
    if(self = [super init]){
        self.base = base;
        self.height = height;
        return self;
    }
    return nil;
}

-(CGFloat)area{
    return self.base*self.height;
}

-(CGFloat)perimeter{
    return 2 * self.base + 2 * self.height;
}

-(BOOL)intersectWith:(Rectangle*)other{
    
    CGFloat x_self1, x_self2, y_self1, y_self2;
    CGFloat x_other1, x_other2, y_other1, y_other2;
    
    x_self1 = self.center.x - (self.base/2);
    x_self2 = self.center.x - (self.base/2);
    y_self1 = self.center.y - (self.height/2);
    y_self2 = self.center.y - (self.height/2);
    
    x_other1 = other.center.x - (other.base/2);
    x_other2 = other.center.x - (other.base/2);
    y_other1 = other.center.y - (other.height/2);
    y_other2 = other.center.y - (other.height/2);
    
    if(x_self2 < x_other1){
        return NO;
    }else if(x_self1 > x_other1){
        return NO;
    }else if(y_self2 > y_other1){
        return NO;
    }else if(y_self1 < y_other2){
        return NO;
    }
    
    return YES;
}

-(NSString *)description{

    
    NSString *s = [NSString stringWithFormat:@" Base: %.2f Height: %.2f", self.base, self.height];
    
    return [[super description]stringByAppendingString:s];
}


@end
