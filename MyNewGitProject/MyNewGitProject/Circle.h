//
//  Circle.h
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Shape.h"

@interface Circle : Shape

@property CGFloat radius;

-(instancetype)initWithRadius:(CGFloat)radius;

@end
