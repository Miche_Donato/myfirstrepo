//
//  Shape.h
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Shape : NSObject

@property CGPoint center;
@property UIColor* color;

-(CGFloat)area;
-(CGFloat)perimeter;
-(BOOL)intersectWith:(Shape*)other;

+(int)numberOfAllocatedReacts;

@end
