//
//  Square.h
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Rectangle.h"

@interface Square : Rectangle

@property CGFloat side;

-(instancetype)initWithSide:(CGFloat)side;

@end
