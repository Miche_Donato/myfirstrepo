//
//  Square.m
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Square.h"

@implementation Square

-(instancetype)initWithSide:(CGFloat)side{
    
//    if(self = [super init]){
//        self.side = side;
//        return self;
//    }
//    return nil;
    
    return [super initWithBase:side andHeight:side];
}

-(CGFloat)side{
    return self.base;
}

-(void)setSide:(CGFloat)side{
    self.base = side;
    self.height = side;
}

@end
