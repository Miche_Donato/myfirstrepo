//
//  CustomView.h
//  MyNewGitProject
//
//  Created by Adecco on 30/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UIView

-(void)addRect:(CGRect)r;

@end
