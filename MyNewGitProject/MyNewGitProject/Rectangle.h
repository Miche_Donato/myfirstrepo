//
//  MyRectangle.h
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Shape.h"

@interface Rectangle : Shape

/* VECCHIO STILE
 {
 CGPoint origin;
 CGFloat base;
 CGFloat height;
 }
 */

// MODERN SYNTAX
//@property CGPoint origin;
@property CGFloat base;
@property CGFloat height;

-(instancetype)initWithBase:(CGFloat)base andHeight: (CGFloat)height;

@end
