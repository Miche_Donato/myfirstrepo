//
//  Shape.m
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Shape.h"

static int count = 0;

@implementation Shape

-(Shape*)init{
    
    if(self =[super init]){
        count++;
        return self;
    }
    return nil;
    
}

-(void)dealloc{
    count--;
}

+(int)numberOfAllocatedReacts{
    return count;
}

-(NSString *)description{
    
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat alpha;
    
    [self.color getRed:&red green:&green blue:&blue alpha:&alpha];
    
    NSString *s = [NSString stringWithFormat:@"X: %.2f Y: %.2f Color: %.2f %.2f %.2f %.2f ", self.center.x, self.center.y, red, green, blue, alpha];
    return s;
}

@end
