//
//  CustomView.m
//  MyNewGitProject
//
//  Created by Adecco on 30/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "CustomView.h"

#define MAX_RECTS 10
#define MAX_CIRCLES 10

@interface CustomView (){
    
    int rectCount;
    int circleCount;

    CGRect rectangle[MAX_RECTS];
    CGRect circle[MAX_CIRCLES];
}

@end

@implementation CustomView

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        rectCount = 0;
        circleCount = 0;
        return self;
    }
    return nil;
}

-(void)addRect:(CGRect)r{
    if(rectCount < MAX_RECTS){
        rectangle[rectCount++] = r;
    }
}

-(void)drawRect:(CGRect)rect{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for(int i = 0; i < rectCount; i++){
        CGContextStrokeRect(context, rectangle[i]);
    }
    
    
    
    
    /*
    CGRect r1 = CGRectMake(20, 20, 200, 200);
    CGContextSetRGBFillColor(context, 1, 0, 0, 1);
    CGContextFillRect(context, r1);
    
    CGRect circle = CGRectMake(30, 40, 100, 100);
    CGContextSetRGBFillColor(context, 0, 0, 1, 1);
    CGContextSetRGBStrokeColor(context, 1, 1, 0, 1);
    CGContextSetLineWidth(context, 4);
    // per creare un cerchio dobbiamo dargli il quadrato circoscritto
    CGContextFillEllipseInRect(context, circle);
    CGContextStrokeEllipseInRect(context, circle);
    
    // line
    CGContextSetRGBStrokeColor(context, 0, 1, 0.5, 1);
    CGContextSetLineWidth(context, 2);
    CGContextMoveToPoint(context, 20, 20);
    CGContextAddLineToPoint(context, 200, 200);
    CGContextAddLineToPoint(context, 20, 200);
    CGContextAddLineToPoint(context, 20, 220);
    CGContextAddLineToPoint(context, 220, 220);
    CGContextAddLineToPoint(context, 220, 20);
    CGContextAddLineToPoint(context, 20, 20);
    // and now draw the line path
    CGContextStrokePath(context);
    
    for(int i = 0; i < 10; i++){
        CGRect r2 = CGRectMake(100+i*10, 100+i*10, 100, 100);
        CGContextSetRGBStrokeColor(context, 0, i/10.0, 0, 1);
        CGContextStrokeRect(context, r2);
    }
    */
    
}

@end
