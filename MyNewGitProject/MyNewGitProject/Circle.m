//
//  Circle.m
//  MyNewGitProject
//
//  Created by Adecco on 29/11/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "Circle.h"
#import <math.h>

@implementation Circle

-(instancetype)initWithRadius:(CGFloat)radius{
    if(self = [super init]){
        self.radius = radius;
        return self;
    }
    return nil;
}

-(CGFloat)area{
    return self.radius * self.radius * M_PI;
}

-(CGFloat)perimeter{
    return 2 * self.radius * M_PI;
}

-(BOOL)intersectWith:(Circle *)other{
    
    CGFloat x_self = self.center.x;
    CGFloat y_self = self.center.y;
    CGFloat x_other = other.center.x;
    CGFloat y_other = other.center.y;
    
    CGFloat cateto1 = x_self - x_other;
    CGFloat cateto2 = y_self - y_other;
    CGFloat distance = sqrt(cateto1*cateto1-cateto2*cateto2);
    
    return distance < self.radius + other.radius;
}

-(NSString *)description{
    
    NSString *s = [NSString stringWithFormat:@"Radius: %.2f", self.radius];
    
    return [[super description]stringByAppendingString:s];
}



@end
