//
//  Place.h
//  NSURLSessionSample
//
//  Created by Adecco on 06/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

@interface Place : NSObject <MKAnnotation>

@property NSString *formatted_address;
@property NSString *address;
@property NSString *city;
@property NSString *prov;
@property NSString *region;
@property NSString *country;
@property CLLocationCoordinate2D coords;

-(instancetype)initWithDictFromJSON:(NSDictionary *)d;

@end
