//
//  MapViewController.h
//  NSURLSessionSample
//
//  Created by Adecco on 06/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
@import MapKit;

@interface MapViewController : UIViewController

-(void)getPlace:(Place *)p;

@end
