//
//  ViewController.m
//  NSURLSessionSample
//
//  Created by Adecco on 06/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "ViewController.h"
#import "Place.h"
#import "MapViewController.h"

@interface ViewController ()

@property NSMutableArray *places;

@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.places = [[NSMutableArray alloc]init];
    [self downloadData];
}

-(void)downloadData{
    
    #define GOOGLE_URL @"https://maps.googleapis.com/maps/api/geocode/json"
    //NSString *address = @"viale+liguria";
    NSString *address = @"piazza-duomo";
    NSString *options = @"sensor=true";
    
    NSString *stringUrl = [NSString stringWithFormat:@"%@?address=%@&%@", GOOGLE_URL, address, options];
    //1 Create URL
    NSURL *map_url = [NSURL URLWithString:stringUrl];
    //2 Request HTTP
    NSURLRequest *request = [NSURLRequest requestWithURL:map_url];
    //3 Create Session: (is a shared session)
    NSURLSession *session = [NSURLSession sharedSession];
    //4 Create a DataTask
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(error!=nil){
            NSLog(@"%@", error);
        }else{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"Status code: %ld", [httpResponse statusCode]);
            [self parse:data];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
        
    }];
    //5 Resume the task
    [task resume];
}

-(void)parse:(NSData *)data{
    
    NSString *s = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    if(s == nil){
        s = [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding];
    }
    //NSLog(@"%@", s);
    
    //Per scaricare un immagine
    //UIImage *img = [[UIImage alloc]initWithData:data];
    
    //BOOL valid = [NSJSONSerialization isValidJSONObject:data];
    
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    //NSLog(@"%@", dict);
    NSArray *results = dict[@"results"];
    //NSLog(@"%@", results);
    
    for(NSDictionary *d in results){
        Place *p = [[Place alloc]initWithDictFromJSON:d];
        if(p){
            [self.places addObject:p];
        }
    }
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.places.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"paninoCell" forIndexPath:indexPath];
    
    // Configure the cell...
    Place *p = self.places[indexPath.row];
    cell.textLabel.text = p.description;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[self hide: NO];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    MapViewController *mapViewC = [segue destinationViewController];
    
    NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
    
    Place *p = self.places[indexPath.row];
    [mapViewC getPlace:p];
}

@end
