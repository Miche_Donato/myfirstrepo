//
//  MapViewController.m
//  NSURLSessionSample
//
//  Created by Adecco on 06/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

#import "MapViewController.h"


@interface MapViewController () <MKMapViewDelegate>

@property Place *place;
@property (weak, nonatomic) IBOutlet MKMapView *myMap;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.myMap addAnnotation:self.place];

    CLLocationCoordinate2D initialPos = self.place.coords;
    [self goHere: initialPos];
    
}

-(void)goHere:(CLLocationCoordinate2D)initialPos{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = MKCoordinateRegionMake(initialPos, span);
    [self.myMap setRegion:region];
}


-(void)getPlace:(Place *)p{
    self.place = p;
}

#pragma mark - MapDelegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    //MKAnnotationView *av = [[MKAnnotationView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[self.myMap dequeueReusableAnnotationViewWithIdentifier:@"PIN_ID"];
    
    if(pinView == nil){
        pinView = [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"PIN_ID"];
        pinView.canShowCallout = YES;
        
        UIImage *image = [UIImage imageNamed:@"ic_place"];
        pinView.image = image;
    }else{
        pinView.annotation = annotation;
    }
    
    return pinView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
