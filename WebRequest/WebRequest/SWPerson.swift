//
//  SWPerson.swift
//  WebRequest
//
//  Created by Adecco on 14/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import Foundation

class SWPerson: NSObject, NSCoding {
    
    private var _name: String!
    private var _height: String!
    private var _mass: String!
    private var _films: [String]!
    
    // Getter
    var name: String { return _name }
    var height: String { return _height }
    var mass: String { return _mass }
    var films: [String] { return _films }
    
    init(name: String, height: String, mass: String, films: [String]){
        _name = name
        _height = height
        _mass = mass
        _films = films
    }
    
    override init(){
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        _name = aDecoder.decodeObject(forKey: "name") as! String
        _height = aDecoder.decodeObject(forKey: "height") as! String
        _mass = aDecoder.decodeObject(forKey: "mass") as! String
        _films = aDecoder.decodeObject(forKey: "films") as! [String]
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_name, forKey: "name")
        aCoder.encode(_height, forKey: "height")
        aCoder.encode(_mass, forKey: "mass")
        aCoder.encode(_films, forKey: "films")
    }
    
}
