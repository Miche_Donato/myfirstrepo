//
//  SWCellTableViewCell.swift
//  WebRequest
//
//  Created by Adecco on 15/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class SWCellTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mass: UILabel!
    @IBOutlet weak var height: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(p: SWPerson) {
        name.text = p.name
        mass.text = "\(p.mass) kg"
        height.text = "\(p.height) cm"
    }

}
