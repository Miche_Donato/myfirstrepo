//
//  DetailsViewController.swift
//  WebRequest
//
//  Created by Adecco on 15/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var massHeight: UILabel!
    @IBOutlet weak var table: UITableView!
    
    var person: SWPerson?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        table.delegate = self
        table.dataSource = self
        
        configureCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let films:[String] = person!.films {
            return films.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath)
        
        cell.textLabel?.text = person?.films[indexPath.row]
        
        return cell
    }
    
    func configureCell() {
        nameLabel.text = person!.name
        massHeight.text = "\(person!.mass) Kg \(person!.height) cm"
    }

}
