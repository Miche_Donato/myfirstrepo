//
//  ViewController.swift
//  WebRequest
//
//  Created by Adecco on 14/12/16.
//  Copyright © 2016 Adecco. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var urlSW = "http://swapi.co/api/people/"
    
    @IBOutlet weak var table: UITableView!
    
    let segueID = "DetailSegue"
    
    var people = [SWPerson]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("viewDidLoad")
        
        table.delegate = self
        table.dataSource = self
        
        if let savedData = UserDefaults.standard.value(forKey: "people") as? Data{
            if let savePeople = NSKeyedUnarchiver.unarchiveObject(with: savedData) as? [SWPerson]{
                self.people = savePeople
                print("Load data")
            }
        }else{
            downloadData()
        }
    }
    
    func downloadData(){
        
        let session = URLSession.shared
        
        if let url: URL = URL(string: urlSW) {
            session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)
                        
                        if let extDict = json as? Dictionary<String, Any> {
                            
                            if let results = extDict["results"] as? [Dictionary<String, Any>] {
                                
                                for dictionary in results {
                                    
                                    if let name = dictionary["name"] as? String, let height = dictionary["height"] as? String, let mass = dictionary["mass"] as? String, let films = dictionary["films"] as? [String] {
                                        
                                        let person = SWPerson(name: name, height: height, mass: mass, films: films)
                                        self.people.append(person)
                                        
                                        print("Data loaded successfully\n\(self.people)")
                                        
                                    }else{
                                        print("Wrong data format")
                                    }
                                }
                                
                                let peopleData = NSKeyedArchiver.archivedData(withRootObject: self.people)
                                UserDefaults.standard.set(peopleData, forKey: "people")
                                UserDefaults.standard.synchronize()
                                
                                self.table.reloadData()
                            }
                            
                        }
                        
                    }catch {
                        print("JSON Serialization failed")
                    }
                }else{
                    print("Data request failed.\nError: \(error)")
                }
            }.resume()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SWCELL", for: indexPath) as! SWCellTableViewCell
        
        cell.configureCell(p: people[indexPath.row])
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == segueID {
            let detailSegue = segue.destination as! DetailsViewController
            
            if let row = table.indexPathForSelectedRow?.row {
                detailSegue.person = people[row]
            }
        }
    }
}

